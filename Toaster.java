class Toaster {
	private String color;
	private int maxWatt;
	private double price;
	
	
	//Constructor
	public Toaster(String color, int maxWatt, double price){
		this.color = color;
		this.maxWatt = maxWatt;
		this.price = price;
	}
	
	//setter methods
	public void setColor(String color){
			this.color = color;
	}
	public void setMaxWatt(int maxWatt){
		this.maxWatt = maxWatt;
	}
	
	//getter methods
	public String getColor(){
		return this.color;
	}
	public int getMaxWatt(){
		return this.maxWatt;
	}
	public double getPrice(){
		return this.price;
	}
	
	//2 methods instance for price and watt
	public void wattRange(){
		if(this.maxWatt < 800){
			System.out.println("Your toaster's max Watt is low");
		}
		else if (this.maxWatt >= 800 && this.maxWatt <= 1500){
			System.out.println("Your toaster's max Watt is average");
		}
		else {
			System.out.println("your toaster's max Watt is above average!");
		}
	}

	public void priceRange(){
		if(this.price < 20){
			System.out.println("Your toaster's price is lower than average");
		}
		else if (this.price >= 20 && this.price <= 40){
			System.out.println("Your toaster's price is average");
		}
		else {
			System.out.println("your toaster's price is abore average!");
		}
	}
	
	
	// instance method with private helper method
	public void colorChoice(String color){
		if(isValid(color)){
			System.out.println("You have a regular toaster color");
		}
		else {
			System.out.println("Your toaster has a unique color!");
		}
	}
	private boolean isValid(String color){
		boolean isValidType = (color.equals("white") || color.equals("black"));
			return isValidType;
	}
}