import java.util.Scanner;
class ApplianceStore {
	public static void main(String[]args){
		
		Scanner keyboard = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		for(int i=0, j=1; i<toasters.length; i++, j++){
			System.out.println("what is the color of your " + j + " toaster?");
			String color = keyboard.next();
			System.out.println("what is the max Watt of your " + j + " toaster?");
			int maxWatt = keyboard.nextInt();
			System.out.println("what is the price of your " + j + " toaster?");
			double price = keyboard.nextDouble();
			toasters[i] = new Toaster(color, maxWatt, price);
		}
		System.out.println("");
		
		//before calling setter
		System.out.println("The color of your last appliance is " + toasters[toasters.length-1].getColor());
		System.out.println("The maxWatt of your last appliance is " + toasters[toasters.length-1].getMaxWatt());
		System.out.println("The price of your last appliance is " + toasters[toasters.length-1].getPrice());
		System.out.println("");
		
		//updating the values of the last appliance value
		System.out.println("Please enter a color to update your last toaster");
		toasters[toasters.length-1].setColor(keyboard.next());
		System.out.println("Please enter a maxWatt to update your last toaster");
		toasters[toasters.length-1].setMaxWatt(keyboard.nextInt());
		System.out.println("");
		
		//after calling setter
		System.out.println("The color of your last appliance is " + toasters[toasters.length-1].getColor());
		System.out.println("The maxWatt of your last appliance is " + toasters[toasters.length-1].getMaxWatt());
		System.out.println("The price of your last appliance is " + toasters[toasters.length-1].getPrice());
		System.out.println("");
		
		toasters[0].wattRange();
		toasters[0].priceRange();
		toasters[1].colorChoice("blue");
	} 
}